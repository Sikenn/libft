/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 10:39:23 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 11:02:20 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		get_itoa_len(unsigned long n, int base)
{
	int len;

	len = 1;
	while (n /= (unsigned long)base)
		len++;
	return (len);
}

void	set_mask(t_printf *p)
{
	if (p->prec < 0)
	{
		p->mask &= ~(1 << PREC);
		p->prec = 0;
	}
	if (p->mask & 1 << NEG && p->mask & 1 << ZERO)
		p->mask &= ~(1 << 1);
	else if (!(p->mask & 1 << NEG) && !(p->mask & 1 << ZERO) && p->width > 0)
	{
		p->mask |= 1 << 2;
	}
}
