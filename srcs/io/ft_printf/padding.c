/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   padding.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/10 10:53:50 by sikenn            #+#    #+#             */
/*   Updated: 2020/03/02 15:35:10 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		padding_cs(t_printf *p)
{
	int		len;

	if ((p->mask & 1 << WIDTH) || (p->mask & 1 << ZERO))
	{
		len = p->width - p->str_len;
		c_to_buf(p, len, (p->mask & 1 << 2 ? ' ' : '0'));
		str_to_buf(p);
	}
	else if (p->mask & 1 << NEG)
	{
		str_to_buf(p);
		len = p->width - p->str_len;
		c_to_buf(p, len, ' ');
	}
	else
		str_to_buf(p);
}

static void	get_true_padding(t_printf *p)
{
	if (p->mask & 1 << PREC)
	{
		if (p->prec > p->str_len)
			p->prec = p->prec - p->str_len;
		else
			p->prec = 0;
	}
	if (p->mask & 1 << WIDTH || p->mask & 1 << ZERO || p->mask & 1 << NEG)
	{
		if (p->width > (p->prec + p->str_len + p->rest))
			p->width = p->width - (p->prec + p->str_len + p->rest);
		else
			p->width = 0;
	}
}

void		padding_diux(t_printf *p, char *base)
{
	if (p->mask & 1 << PREC && p->prec == 0 && p->digit == 0)
		p->str_len = 0;
	get_true_padding(p);
	if ((p->mask & 1 << ZERO && p->mask & 1 << PREC) || p->mask & 1 << WIDTH)
		c_to_buf(p, p->width, ' ');
	if (p->rest == 1)
		c_to_buf(p, 1, '-');
	if (p->rest == 2)
	{
		c_to_buf(p, 1, '0');
		c_to_buf(p, 1, 'x');
	}
	if (p->mask & 1 << ZERO && !(p->mask & 1 << PREC))
		c_to_buf(p, p->width, '0');
	if (p->prec > 0)
		c_to_buf(p, p->prec, '0');
	if (p->str_len > 0)
		bufnbr_base(p, p->digit, base);
	if (p->mask & 1 << 0)
		c_to_buf(p, p->width, ' ');
}
