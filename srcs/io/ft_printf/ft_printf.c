/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/09 12:57:29 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 11:01:37 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			get_conv(t_printf *p, int index)
{
	const t_conv	convs[NB_CONVS] = {&conv_c, &conv_s, &conv_p,
			&conv_di, &conv_di, &conv_u, &conv_x, &conv_bigx, &conv_perc};

	convs[index](p);
	p->prec = 0;
	p->width = 0;
	p->nbr = 0;
	p->digit = 0;
	p->mask = 0;
}

int				letter(char *fmt, t_printf *p, t_machine *machine)
{
	if (*fmt == FORMAT)
		machine->state = GET_FORMAT;
	else
		c_to_buf(p, 1, *fmt);
	return (1);
}

static int		parser(char *fmt, t_printf *p, t_machine *machine)
{
	int				ret;
	const t_state	state[6] = {letter, get_format,
			flag, value, precision, conv};

	p->mask = 0;
	machine->state = LETTER;
	while (*fmt)
	{
		ret = state[machine->state](fmt, p, machine);
		if (ret == FAILURE)
			return (FAILURE);
		fmt += ret;
	}
	return (SUCCESS);
}

int				ft_printf(const char *fmt, ...)
{
	t_printf		p;
	t_machine		machine;

	ft_bzero(&p, sizeof(t_printf));
	va_start(p.ap, fmt);
	if (parser((char *)fmt, &p, &machine) == FAILURE)
		return (FAILURE);
	va_end(p.ap);
	print_buf(&p);
	return (p.t_size);
}
