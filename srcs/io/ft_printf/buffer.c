/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   buffer.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 11:44:28 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 11:02:01 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		clean_buf(t_printf *p)
{
	ft_bzero(p->buf, BUFFER_SIZE);
	p->size = 0;
}

void		print_buf(t_printf *p)
{
	if (p->size > 0)
	{
		write(1, p->buf, (size_t)p->size);
		clean_buf(p);
	}
}

void		c_to_buf(t_printf *p, int len, int carac)
{
	int i;

	i = 0;
	while (i < len)
	{
		if (p->size + i > BUFFER_SIZE)
			print_buf(p);
		p->buf[p->size] = (char)carac;
		p->size++;
		p->t_size++;
		i++;
	}
}

void		str_to_buf(t_printf *p)
{
	int i;

	i = 0;
	while (i < p->str_len)
	{
		if (p->size + i > BUFFER_SIZE)
			print_buf(p);
		p->buf[p->size] = p->str[i];
		i++;
		p->size++;
		p->t_size++;
	}
}

void		bufnbr_base(t_printf *p, unsigned long n, char *base)
{
	unsigned long	nbr;

	nbr = n;
	if (nbr >= p->len_base)
		bufnbr_base(p, nbr / p->len_base, base);
	c_to_buf(p, 1, base[nbr % p->len_base]);
}
