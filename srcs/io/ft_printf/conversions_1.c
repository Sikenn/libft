/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversions_1.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/06 17:32:59 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 11:01:46 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	conv_c(t_printf *p)
{
	char	c[1];

	*c = (char)va_arg(p->ap, int);
	p->str = c;
	p->str_len = 1;
	padding_cs(p);
}

void	conv_s(t_printf *p)
{
	static char	*s = "(null)";

	p->str = va_arg(p->ap, char *);
	if (!p->str)
	{
		p->str = s;
		p->str_len = 6;
	}
	else
		p->str_len = (int)ft_strlen(p->str);
	if (p->mask & 1 << 3 && p->prec < p->str_len)
		p->str_len = p->prec;
	padding_cs(p);
}

void	conv_p(t_printf *p)
{
	p->digit = va_arg(p->ap, unsigned long);
	p->str_len = get_itoa_len(p->digit, 16);
	p->rest = 2;
	p->len_base = ft_strlen(HEXA);
	padding_diux(p, HEXA);
}

void	conv_x(t_printf *p)
{
	p->digit = va_arg(p->ap, unsigned long);
	p->digit = (unsigned int)p->digit;
	p->str_len = get_itoa_len(p->digit, 16);
	p->rest = 0;
	p->len_base = ft_strlen(HEXA);
	padding_diux(p, HEXA);
}

void	conv_bigx(t_printf *p)
{
	p->digit = va_arg(p->ap, unsigned long);
	p->digit = (unsigned int)p->digit;
	p->str_len = get_itoa_len(p->digit, 16);
	p->rest = 0;
	p->len_base = ft_strlen(BIG_HEXA);
	padding_diux(p, BIG_HEXA);
}
