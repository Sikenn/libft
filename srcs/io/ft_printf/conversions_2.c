/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversions_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/06 17:33:05 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 11:01:53 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	conv_di(t_printf *p)
{
	p->nbr = va_arg(p->ap, int);
	p->rest = 0;
	if (p->nbr < 0)
	{
		p->rest = 1;
		p->nbr *= -1;
	}
	p->digit = (unsigned int)p->nbr;
	p->str_len = get_itoa_len(p->digit, 10);
	p->len_base = ft_strlen(U_DECI);
	padding_diux(p, U_DECI);
}

void	conv_u(t_printf *p)
{
	p->digit = va_arg(p->ap, unsigned long);
	p->digit = (unsigned int)p->digit;
	p->str_len = get_itoa_len(p->digit, 10);
	p->rest = 0;
	p->len_base = ft_strlen(U_DECI);
	padding_diux(p, U_DECI);
}

void	conv_perc(t_printf *p)
{
	char	c[1];

	*c = '%';
	p->str = c;
	p->str_len = 1;
	padding_cs(p);
}
