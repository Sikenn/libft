/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/16 11:20:10 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 14:34:30 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		conv(char *fmt, t_printf *p, t_machine *machine)
{
	int	i;

	i = 0;
	while (i < NB_CONVS)
	{
		if (*fmt == CONVS[i])
			break ;
		i++;
	}
	machine->state = LETTER;
	if (i == NB_CONVS)
		return (0);
	else
	{
		set_mask(p);
		get_conv(p, i);
		return (1);
	}
}

int		precision(char *fmt, t_printf *p, t_machine *machine)
{
	int	i;

	i = 0;
	while (fmt[i] == CARAC_PRECISION)
		i++;
	if (fmt[i] == STAR)
	{
		p->prec = va_arg(p->ap, int);
		i++;
	}
	if (ft_isdigit(fmt[i]))
		p->prec = ft_atoi(fmt + i);
	while (ft_isdigit(fmt[i]))
		i++;
	p->mask |= 1 << 3;
	machine->state = GET_FORMAT;
	return (i);
}

int		value(char *fmt, t_printf *p, t_machine *machine)
{
	int i;

	i = 0;
	while (fmt[i] == STAR)
	{
		p->width = va_arg(p->ap, int);
		if (p->width < 0)
		{
			p->mask |= 1 << 0;
			p->width *= -1;
		}
		i++;
	}
	if (ft_isdigit(fmt[i]))
		p->width = ft_atoi(fmt + i);
	while (ft_isdigit(fmt[i]))
		i++;
	machine->state = GET_FORMAT;
	return (i);
}

int		flag(char *fmt, t_printf *p, t_machine *machine)
{
	int i;
	int	j;

	i = 0;
	j = 0;
	while (i < NB_FLAGS)
	{
		if (fmt[j] == FLAGS[i])
		{
			while (fmt[j] == FLAGS[i])
				j++;
			p->mask |= (1 << i);
			break ;
		}
		i++;
	}
	machine->state = GET_FORMAT;
	return (j);
}

int		get_format(char *fmt, t_printf *p, t_machine *machine)
{
	(void)p;
	if (*fmt == CARAC_PRECISION)
		machine->state = PRECISION;
	else if (ft_strchr(VALUES, *fmt))
		machine->state = VALUE;
	else if (ft_strchr(FLAGS, *fmt))
		machine->state = FLAG;
	else
		machine->state = CONV;
	return (0);
}
