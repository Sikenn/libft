/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 14:51:47 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 18:29:44 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void		get_rest(t_file *file)
{
	char	*tmp;
	char	*tmp_free;

	if (file->rest)
	{
		tmp_free = file->rest;
		if ((tmp = ft_strchr(file->rest, '\n')))
		{
			file->rest[ft_strlen(file->rest) - ft_strlen(tmp)] = '\0';
			file->cur = ft_strdup(file->rest);
			file->rest = ft_strdup(tmp + 1);
			file->state = SUCCESS;
		}
		else
		{
			file->cur = ft_strdup(file->rest);
			file->state = END;
		}
		free(tmp_free);
	}
}

static int		read_file(t_file *file)
{
	int		ret;
	char	*tmp;
	char	*tmp_free;

	while ((ret = read(file->fd, file->buf, BUFFER_SIZE)) > 0)
	{
		file->buf[ret] = '\0';
		tmp_free = file->cur;
		if ((tmp = ft_strchr(file->buf, '\n')))
		{
			file->rest = ft_strdup(tmp + 1);
			file->buf[ret - ft_strlen(tmp)] = '\0';
			file->cur = ft_strjoin(file->cur, file->buf);
			free(tmp_free);
			return (SUCCESS);
		}
		file->cur = ft_strjoin(file->cur, file->buf);
		free(tmp_free);
	}
	if (ret < 0)
		return (FAILURE);
	else if (ret < BUFFER_SIZE)
		return (END);
	else
		return (ret == FAILURE ? FAILURE : SUCCESS);
}

int				get_next_line(int fd, char **line)
{
	static t_file	file;

	if (fd >= 0 && line != NULL && BUFFER_SIZE > 0)
	{
		file.fd = fd;
		get_rest(&file);
		if (file.state != SUCCESS)
		{
			file.state = read_file(&file);
			if (file.state == FAILURE)
				return (FAILURE);
		}
		if (file.cur != NULL)
		{
			*line = ft_strdup(file.cur);
			free(file.cur);
		}
		else
			*line = ft_strdup("");
		return (file.state);
	}
	else
		return (FAILURE);
}
