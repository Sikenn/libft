/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr_nbr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/28 09:15:56 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/28 09:53:05 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_strchr_nbr() function loactes the first digit on the string pointed
**  to by s.
**
** RETURN VALUES
**  The ft_strchr_nbr() function return a pointer to the located first digit,
**  or NULL if the cracter does not appear in the string.
*/

char	*ft_strchr_nbr(const char *s)
{
	if (s == NULL)
		return (NULL);
	while (!ft_isdigit(*s) && *s != '\0')
		s++;
	if (*s == '\0')
		return (NULL);
	return ((char*)s);
}
