/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/21 17:43:17 by sikenn            #+#    #+#             */
/*   Updated: 2019/11/23 15:02:14 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_memchr() function locates the first occurrence of c (converted to
**  an unsigned char) in string s.
**
** RETURN VALUES
**  The ft_memchr() function returns a pointer to the byte located, or NULL
**  if no such byte exists within n bytes.
*/

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t				i;
	const unsigned char	*cs;

	cs = s;
	i = 0;
	while (cs && *cs && *cs != (unsigned char)c && i < n)
	{
		i++;
		cs++;
	}
	if (i < n && *cs == (unsigned char)c)
		return ((void*)cs);
	else
		return (NULL);
}
