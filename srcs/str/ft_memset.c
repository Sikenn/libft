/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/21 16:36:37 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 11:00:10 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_memset() function writes len bytes of value c (converted to an
**  unsigned char) to the string b.
**
** RETURN VALUE
**  The ft_memset() function returns its first argument.
*/

void	*ft_memset(void *s, int c, size_t len)
{
	while (len--)
		((char *)s)[len] = (unsigned char)c;
	return (s);
}
