/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/22 18:17:06 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/25 13:20:58 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The ft_strdup() function allocates sufficient memory for a copy of the
** string s1, does the copy, and returns a pointer to it. The pointer may
** subsequently be used as an argument the the function free(3).
**
** If insufficient memory is availiable, NULLis returned and errno is set to
** ENOMEN.
*/

char	*ft_strdup(const char *s1)
{
	char	*str;	
	size_t	len;

	len = ft_strlen(s1) + 1;
	if (!(str = (char *)malloc(len * sizeof(char))))
		return (NULL);
	ft_memcpy(str, s1, len);
	return (str);
}
