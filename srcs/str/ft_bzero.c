/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/21 16:43:09 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 11:00:37 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_bzero() function writes n zeroed bytes to the string s. If n is
**  zero, bzero() does nothing.
*/

void	ft_bzero(void *s, size_t len)
{
	ft_memset(s, 0, len);
}
