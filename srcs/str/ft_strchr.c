/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 10:09:10 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/28 09:53:06 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_strchr() function locates the first occurence of c (converted to
**  a char) in the string pointed to by s. The terminating null character is
**  considered to be part of the string; therefore if c is '\0', the functions
**  locate the terminating '\0'.
**
** RETURN VALUES
**  The functions ft_strchr() return a pointer to the located character, or
**  NULL if the character does not appear in the string.
*/

char	*ft_strchr(const char *s, int c)
{
	if (s == NULL)
		return (NULL);
	while (*s != c && *s != '\0')
		s++;
	if (*s == '\0' && c != 0)
		return (NULL);
	return ((char*)s);
}
