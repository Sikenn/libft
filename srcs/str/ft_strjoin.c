/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/22 18:21:30 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/25 13:59:28 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char *s1, char *s2)
{
	char	*str;
	size_t	len_s1;
	size_t	len_s2;

	if (!s1 && !s2)
		return (NULL);
	else if (!s1)
		str = ft_strdup(s2);
	else if (!s2)
		str = ft_strdup(s1);
	else
	{
		len_s1 = ft_strlen(s1);
		len_s2 = ft_strlen(s2);
		if (!(str = (char *)malloc((len_s1 + len_s2 + 1) * sizeof(char))))
			return (NULL);

		str[len_s1 + len_s2 + 1] = '\0';
		ft_memcpy(str, s1, len_s1);
		ft_memcpy(str + len_s1, s2, len_s2);
	}
	return (str);
}
