/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/22 18:14:22 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 18:28:15 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memcpy() function copies n bytes from memory area src to memory area dst.
** If dst and src overlap, behavior is undefined. Applications in which dst and
** src might overlap should use memmove instead.
*/

void	*ft_memcpy(void *dst, const void *src, size_t len)
{
	while (len--)
		((char *)dst)[len] = ((char *)src)[len];
	return (dst);
}
