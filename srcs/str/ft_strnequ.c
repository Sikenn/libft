/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/25 11:31:16 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/25 11:36:35 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_strnequ function compare s1 and s2 but not more than len caracthers
**
** RETURN VALUE
**  The ft_strnequ() function return TRUE if no the string was identic and
**  FALSE in the other case
*/

int		ft_strnequ(const char *s1, const char *s2, size_t len)
{
	if (!s1 && !s2)	
		return (TRUE);
	else if (!s1 || !s2)
		return (FALSE);
	while ((*s1 || *s2) && len--)
	{
		if (*s1++ != *s2++)
			return (FALSE);
	}
	return (TRUE);
}
