/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/25 11:17:42 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/25 12:02:57 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_strcmp and strncmp() functions lexicographically compare the
**  null-terminated strings s1 and s2.
**
**  The ft_strncmp() function compares nat more than n characters. Because
**  ft_strncmp() is designed for comparing strings rather than binary data,
**  characters tha appear a '\0' caracter are not compared.
**
** RETURN VALUE
**  The ft_strcmp() and ft_strncmp() functions return an integer greater than,
**  equal to, or less than 0, according as the string s1 is greater than,
**  equal to, or less than the string s2. the comparison is onde using
**  unsigned characters, so that '\200' is greater than '\0'.
*/

int		ft_strncmp(const char *s1, const char *s2, size_t len)
{
	size_t i;

	i = 0;
	while ((s1[i] || s2[i]) && i < len)
	{
		if (s1[i] != s2[i])
			return ((int)(unsigned char)s1[i] - (unsigned char)s2[i]);
		i++;
	}
	return (0);
}

int		ft_strcmp(const char *s1, const char *s2)
{
	size_t i;
	
	i = 0;
	while (s1[i] && s1[i] == s2[i])
		i++;
	return ((int)(unsigned char)s1[i] - (unsigned char)s2[i]);
}
