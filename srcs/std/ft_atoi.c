/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/28 15:13:25 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 11:23:50 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The ft_atoi() function converts to the initial portion of the sing pointed
** by s to int representation.
*/

int		ft_atoi(const char *s)
{
	int r;
	int m;

	r = 0;
	m = 1;
	while (*s == ' ' || *s == '\t' || *s == '\n'
		|| *s == '\f' || *s == '\r' || *s == '\v')
		s++;
	if (*s == '-')
	{
		m = -1;
		s++;
	}
	else if (*s == '+')
		s++;
	while (*s >= '0' && *s <= '9')
	{
		r = r * 10;
		r += (*s - 48);
		s++;
	}
	return (r * m);
}
