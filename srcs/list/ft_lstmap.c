/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/12 15:22:15 by sikenn            #+#    #+#             */
/*   Updated: 2019/11/12 16:04:55 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** 	Iterates the list and applies the function f to the content of each
** 	element. Creates a new list resulting of the successive applications of
** 	the function f. The del funciton is here to delete the content of an
** 	element if needed.
**
** RETURN VALUE
**  The  new list. NULL if the allocation fails.
*/

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list *res;
	t_list *beg;
	t_list *cur;

	if (!(beg = ft_lstnew(f(lst->content))))
	{
		del(beg);
		return (NULL);
	}
	cur = lst->next;
	while (cur)
	{
		if (!(res = ft_lstnew(f(cur->content))))
		{
			del(res);
			return (NULL);
		}
		else
			ft_lstadd_back(&beg, res);
		cur = cur->next;
	}
	return (beg);
}
