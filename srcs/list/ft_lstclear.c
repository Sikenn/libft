/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstclear.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/30 14:14:54 by sikenn            #+#    #+#             */
/*   Updated: 2019/11/11 15:27:23 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  Deletes and free the given element and every successor of that element,
**  using the function del and free(3).
**	Finally, the pointer to the list must be set to NULL.
**
** RETURN VALUE.
**  None.
*/

void	ft_lstclear(t_list **lst, void (*del)(void*))
{
	t_list *begin;
	t_list *tmp;

	begin = *lst;
	tmp = NULL;
	while (begin)
	{
		if (begin->next)
			tmp = begin->next;
		else
			tmp = NULL;
		ft_lstdelone(begin, del);
		begin = tmp;
	}
	*lst = NULL;
}
