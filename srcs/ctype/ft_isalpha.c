/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   isalpha.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 09:36:17 by sikenn            #+#    #+#             */
/*   Updated: 2019/11/23 15:08:21 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
** 	The ft_isalpha() function tests for any character for zhich ft_isupper() or
** 	ft_islower() is true. The value of the argument must be representable as an
** 	unsigned char or the value of EOF.
**
** RETURN VALUES
**  The ft_isalpha() function returns zero if the character test false and
**  returns non-zero if the character tests true.
*/

int				ft_isalpha(int c)
{
	if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122))
		return (1);
	return (0);
}
