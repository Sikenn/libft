/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 09:58:12 by sikenn            #+#    #+#             */
/*   Updated: 2019/11/23 15:11:04 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_tolower() function converts an upper-case letter to the
**  corresponding lower-case letter. The argument must be representable as an
**  unsigned char or the value of EOF.
**
** RETURN VALUES
**  If the argument is an upper-case letter, the ft_tolower() function returns
**  the corresponding lower-case letter if ther is one; othewise, the argument
**  is returned unchanged.
*/

int				ft_tolower(int c)
{
	if (c >= 65 && c <= 90)
		c += 32;
	return (c);
}
