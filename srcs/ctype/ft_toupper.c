/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 10:04:35 by sikenn            #+#    #+#             */
/*   Updated: 2019/11/23 15:10:54 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_toupper() function converts a lower-case letter to the
**  corresponding upper-case letter. The argument must be representable as an
**  unsigned char or the value of EOF.
**
** RETURN VALUES
**  If the argument is a lower-case letter, the ft_toupper() function returns
**  the corresponding upper-case letter if there is one; otherwise, the
**  argument is returned unchanged.
*/

int				ft_toupper(int c)
{
	if (c >= 97 && c <= 122)
		c -= 32;
	return (c);
}
