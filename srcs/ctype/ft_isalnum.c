/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 09:44:14 by sikenn            #+#    #+#             */
/*   Updated: 2019/11/23 15:09:04 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_isalnum() function tests for any character for which ft_isalpha()
**  or ft_isdigit() is true. The value of the argument must be representable
**  as an unisgned char or the value of EOF.
**
** RETURN VALUES
**  The ft_isalnum() function returns zero if the character tests fales and
**  returns non-zero if the cahracter tests true.
*/

int		ft_isalnum(int c)
{
	return (ft_isalpha(c) || ft_isdigit(c));
}
