/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/22 09:52:31 by sikenn            #+#    #+#             */
/*   Updated: 2019/11/23 15:10:16 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** DESCRIPTION
**  The ft_isprint() function tests for any printing characater, including
**  space. The value of the argument must be representable as an unsigned char
**  or the value of EOF.
**
** RETURN VALUES
**  The ft_isprint() function returns zero if the character tests false and
**  returns non-zero if the character tests true.
*/

int		ft_isprint(int c)
{
	if (c >= 32 && c < 127)
		return (1);
	return (0);
}
