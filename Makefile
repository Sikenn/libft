# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sikenn <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/01/20 17:43:42 by sikenn            #+#    #+#              #
#    Updated: 2020/03/05 14:16:25 by sikenn           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
#
################################################################################
############################### MAIN VARIABLES #################################
################################################################################

# Name
NAME = libft.a

# Compiler
CC = gcc 

# Compiler Flags
CFLAGS += -Wall
CFLAGS += -Wextra
CFLAGS += -Werror

################################################################################
############################### PRINT VARIABLES ################################
################################################################################

# Reset
NC = \033[0m

# Colors
YELLOW = \033[0;33m
GREEN = \033[32m
BLUE = \033[0;34m
RED = \033[0;31m
PURPLE = \033[0;35m
CYAN = \033[0;36m
BLACK = \033[0;30
WHITE = \033[0;37m

# One Line Output
ONELINE =\e[1A\r

################################################################################
############################### INCLUDES #######################################
################################################################################

INCLUDES = includes/
I_INCLUDES += -I $(INCLUDES)

################################################################################
############################### HEADERS ########################################
################################################################################

HEADER += libft.h
HEADER += ctype.h
HEADER += std.h
HEADER += str.h
HEADER += io.h
HEADER += ft_printf.h
HEADER += get_next_line.h
HEADER += list.h

vpath %.h $(INCLUDES)

################################################################################
############################### PATH SOURCES ###################################
################################################################################

PATH_SRCS_PRINTF = srcs/io/ft_printf
PATH_SRCS_CTYPE = srcs/ctype
PATH_SRCS_STR = srcs/str
PATH_SRCS_IO = srcs/io
PATH_SRCS_STD = srcs/std
PATH_SRCS_GNL = srcs/get_next_line
PATH_SRCS_LIST = srcs/list

################################################################################
############################### SOURCES ########################################
################################################################################

# ctype
SRCS += ft_isalnum.c
SRCS += ft_isalpha.c
SRCS += ft_isascii.c
SRCS += ft_isdigit.c
SRCS += ft_isprint.c
SRCS += ft_tolower.c
SRCS += ft_toupper.c

# string
SRCS += ft_memset.c
SRCS += ft_bzero.c
SRCS += ft_strchr.c
SRCS += ft_strlen.c
SRCS += ft_memcpy.c
SRCS += ft_strdup.c
SRCS += ft_strjoin.c
SRCS += ft_strcmp.c
SRCS += ft_strnequ.c
SRCS += ft_strrchr.c
SRCS += ft_strchr_nbr.c
SRCS += ft_strsub.c

# ft_printf
SRCS += ft_printf.c
SRCS += buffer.c
SRCS += parser.c 
SRCS += conversions_1.c
SRCS += conversions_2.c
SRCS += utils.c
SRCS += padding.c

#io
SRCS += ft_putchar.c
SRCS += ft_putstr.c

# get_next_line
SRCS += get_next_line.c

# std
SRCS += ft_atoi.c

# list
SRCS += ft_lstadd_back.c
SRCS += ft_lstadd_front.c
SRCS += ft_lstclear.c
SRCS += ft_lstdelone.c
SRCS += ft_lstiter.c
SRCS += ft_lstlast.c
SRCS += ft_lstmap.c
SRCS += ft_lstnew.c
SRCS += ft_lstsize.c

################################################################################
############################### ATRIBUTION #####################################
################################################################################

vpath %.c $(PATH_SRCS_PRINTF)
vpath %.c $(PATH_SRCS_IO)
vpath %.c $(PATH_SRCS_CTYPE)
vpath %.c $(PATH_SRCS_STR)
vpath %.c $(PATH_SRCS_STD)
vpath %.c $(PATH_SRCS_GNL)
vpath %.c $(PATH_SRCS_LIST)

################################################################################
############################### OBJECTS ########################################
################################################################################

PATH_OBJS = objs/
OBJS = $(patsubst %.c, $(PATH_OBJS)%.o, $(SRCS))

################################################################################
############################### RULES ##########################################
################################################################################

#------------------------------ STANTARD --------------------------------------#

all: $(PATH_OBJS) $(NAME) 

$(NAME): $(OBJS) 
		ar rcs $@ $^
		printf "$(GREEN)$@ is ready.\n\n$(NC)"

$(OBJS): $(PATH_OBJS)%.o: %.c $(HEADER) Makefile
	$(CC) $(CFLAGS) $(I_INCLUDES) -c $< -o $@
	printf "$(ONELINE)$(CYAN)Compiling $<"
	printf "                                                            \n$(NC)"

$(PATH_OBJS):
	mkdir $@

clean:
	$(RM) $(OBJS)
	$(RM) -R $(PATH_OBJS)
	printf "$(RED)OBJS from printf removed\n$(RESET)"

fclean: clean
	$(RM) $(NAME)
	printf "$(RED)$(NAME) removed\n$(RESET)"

re: fclean all


#------------------------------ SPECIAL ---------------------------------------#

test:  
	make -C .
	rm ../ft_printf_unit_test/$(NAME)
	cp $(NAME) ../ft_printf_unit_test/
	make -C ../ft_printf_unit_test/
	./../ft_printf_unit_test/ft_printf_tests

trace:
	vim ../curqui_test/rslt_trace.txt

.PHONY: all clean fclean re
.SILENT:
