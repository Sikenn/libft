/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 07:28:25 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 10:38:25 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

/*
** --------------------- External Library  -------------------------------------
*/

//# include "libft.h"
# include <stdarg.h>

/*
** --------------------- Defines -----------------------------------------------
*/

# define BUFFER_SIZE 4096
# define FORMAT '%'
# define NB_FLAGS 2
# define FLAGS "-0"
# define VALUES "123456789*"
# define NB_CONVS 9
# define CONVS "cspdiuxX%"
# define CARAC_PRECISION '.'
# define STAR '*'
# define HEXA "0123456789abcdef"
# define BIG_HEXA "0123456789ABCDEF"
# define U_DECI "0123456789"
# define FAILURE -1
# define SUCCESS 1

/*
** --------------------- Defines Flags -----------------------------------------
*/

# define BNEG 0x0001
# define BZERO 0x0002
# define BWIDTH 0x0004
# define BPREC 0x0008

# define NEG 0
# define ZERO 1
# define WIDTH 2
# define PREC 3

/*
** ----------------- Structure Definition --------------------------------------
*/

typedef struct		s_printf
{
	char			*str;
	long			nbr;
	unsigned long	digit;
	unsigned long	len_base;
	char			buf[BUFFER_SIZE];
	va_list			ap;
	int				str_len;
	int				precision;
	int				width;
	int				prec;
	int				rest;
	int				size;
	int				t_size;
	int				mask;
}					t_printf;

enum				e_state
{
	LETTER,
	GET_FORMAT,
	FLAG,
	VALUE,
	PRECISION,
	CONV
};

typedef struct		s_machine
{
	enum e_state state;
}					t_machine;

typedef int			(*t_state)(char *, t_printf *, t_machine *);
typedef void		(*t_conv)(t_printf *);

/*
** ----------------- Cores Functions -------------------------------------------
*/

int					ft_printf(const char *fmt, ...);
void				get_conv(t_printf *p, int index);

/*
** ----------------- Buffer Functions  -----------------------------------------
*/

void				clean_buf(t_printf *p);
void				print_buf(t_printf *p);
void				c_to_buf(t_printf *p, int len, int carac);
void				str_to_buf(t_printf *p);
void				bufnbr_base(t_printf *p, unsigned long n, char *base);

/*
** ----------------- Pading Functions  -----------------------------------------
*/

void				padding_cs(t_printf *p);
void				padding_diux(t_printf *p, char *base);

/*
**----------------- Machine State Functions -----------------------------------
*/

int					conv(char *fmt, t_printf *p, t_machine *machine);
int					precision(char *fmt, t_printf *p, t_machine *machine);
int					value(char *fmt, t_printf *p, t_machine *machine);
int					flag(char *fmt, t_printf *p, t_machine *machine);
int					get_format(char *fmt, t_printf *p, t_machine *machine);
int					letter(char *fmt, t_printf *p, t_machine *machine);

/*
**----------------- Conversion Functions ---------------------------------------
*/

void				conv_c(t_printf *p);
void				conv_s(t_printf *p);
void				conv_p(t_printf *p);
void				conv_di(t_printf *p);
void				conv_u(t_printf *p);
void				conv_x(t_printf *p);
void				conv_bigx(t_printf *p);
void				conv_perc(t_printf *p);

/*
**----------------- Utils Functions --------------------------------------------
*/

int					get_itoa_len(unsigned long n, int base);
void				set_mask(t_printf *p);

#endif
