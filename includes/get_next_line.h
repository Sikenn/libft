/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 14:52:31 by sikenn            #+#    #+#             */
/*   Updated: 2020/02/22 18:27:30 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>
# include <string.h>

# define FALSE 0
# define TRUE 1
# define FAILURE -1
# define SUCCESS 1
# define END 0

typedef struct		s_file
{
	int				state;
	int				fd;
	char			*rest;
	char			*cur;
	char			buf[BUFFER_SIZE + 1];
}					t_file;

int					get_next_line(int fd, char **line);

#endif
