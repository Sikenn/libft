/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sikenn <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/22 10:51:52 by sikenn            #+#    #+#             */
/*   Updated: 2020/03/05 14:15:42 by sikenn           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>

# include "str.h"
# include "ft_printf.h"
# include "ctype.h"
# include "std.h"
# include "get_next_line.h"
# include "io.h"
# include "list.h"

#endif
